unit ideal.logger.viewer.stores.views;

interface

uses
  System.Classes,
  System.SysUtils,
  System.Generics.Collections,
  System.UITypes,
  cocinasync.flux.store,
  cocinasync.Async,
  chimera.json,
  ideal.logger.viewer.types,
  ideal.logger.viewer.actions;

type
  TViewsStore = class(TBaseStore)
  strict private
    class var FData : TViewsStore;

  strict private type
    TFilterView = class(TInterfacedObject, IFilterView)
    private
      FStampAfter : TDateTime;
      FStampBefore : TDateTime;
      FTextInclusive : TArray<string>;
      FTextExclusive : TArray<string>;
      FExcludeThreadIDs : TArray<UInt64>;
      FIncludeThreadIDs : TArray<UInt64>;
      FLineFrom : UInt64;
      FLineTo : UInt64;
      FDurationMin : UInt64;
      FDurationMax : UInt64;
      FDeltaMin : UInt64;
      FHideProfileNoDetail : boolean;
      FViews : TList<IFilterView>;
      FParent : IFilterView;

      function GetStampAfter : TDateTime;
      function GetStampBefore : TDateTime;
      function GetTextInclusive : TArray<string>;
      function GetTextExclusive : TArray<string>;
      function GetExcludeThreadIDs : TArray<UInt64>;
      function GetIncludeThreadIDs : TArray<UInt64>;
      function GetLineFrom : UInt64;
      function GetLineTo : UInt64;
      function GetDurationMin : UInt64;
      function GetDurationMax : UInt64;
      function GetDeltaMin : UInt64;
      function GetHideProfileNoDetail : boolean;
      function GetViews : TList<IFilterView>;
      function GetParent : IFilterview;
    public
      constructor Create(const AParent : IFilterView); reintroduce;
      destructor Destroy; override;

      function CompositedView : IFilterView;
      function IsThreadIncluded(ID : UInt64) : boolean;
      function IsFiltered : boolean;

      property StampAfter : TDateTime read GetStampAfter;
      property StampBefore : TDateTime read GetStampBefore;
      property TextInclusive : TArray<string> read GetTextInclusive;
      property TextExclusive : TArray<string> read GetTextExclusive;
      property IncludeThreadIDs : TArray<UInt64> read GetIncludeThreadIDs;
      property ExcludeThreadIDs : TArray<UInt64> read GetExcludeThreadIDs;
      property LineFrom : UInt64 read GetLineFrom;
      property LineTo : UInt64 read GetLineTo;
      property DurationMin : UInt64 read GetDurationMin;
      property DurationMax : UInt64 read GetDurationMax;
      property DeltaMin : UInt64 read GetDeltaMin;
      property HideProfileNoDetail : boolean read GetHideProfileNoDetail;
      property Views : TList<IFilterView> read GetViews;
      property Parent : IFilterView read GetParent;

      procedure ApplyFrom(const AView : IFilterView);
      function Add : IFilterView;
      procedure Remove(const AView : IFilterView);
      function FindView(const AView : IFilterView) : IFilterView;
      procedure IncludeThread(ID : UInt64);
      procedure ExcludeThread(ID : UInt64);
      procedure IncludeText(const Text : string);
      procedure ExcludeText(const Text : string);
      procedure UpdateStampAfter(Stamp : TDateTime);
      procedure UpdateStampBefore(Stamp : TDateTIme);
      procedure UpdateLineFrom(Line : UInt64);
      procedure UpdateLineTo(Line : UInt64);
      procedure UpdateDeltaMin(Value : UInt64);
      procedure UpdateDurationMin(Value : UInt64);
      procedure UpdateDurationMax(Value : UInt64);
      procedure HideProfileWithNoDetail;
      procedure ReparentTo(const View : IFilterView);

      procedure RemoveView(const AView : IFilterView);
      procedure Reverse;

      function AsText : string;
      function AsJSON(Current : IFilterView) : IJSONObject;

      function DateFormat(DT : TDateTime) : string;

      procedure UpdateFromJSON(const Data : IJSONObject);
    end;


  strict private
    FRoot : IFilterView;
    FCurrent : IFilterView;
    FViewFilename : string;
  private
    procedure LoadViewsFromJSON(Data : IJSONObject);
    function PersistViewsToJSON : IJSONObject;

    procedure SaveViewsToFile;
    procedure LoadViewsFromFile(DataFile : TFileRecord);
  public
    constructor Create; override;
    destructor Destroy; override;

    class property Data : TViewsStore read FData;
    class constructor Create;
    class destructor Destroy;

    property Root : IFilterView read FRoot;
    property Current : IFilterView read FCurrent;
  end;

implementation

uses
  System.IOUtils,
  cocinasync.flux,
  cocinasync.flux.fmx.actions.ui;

{ TViewsStore }

constructor TViewsStore.Create;
begin
  inherited Create;
  FRoot := TFilterView.Create(nil);
  FCurrent := FRoot;

  Flux.Register<TLoadViewsForFileAction>(Self,
    procedure(Action : TLoadViewsForFileAction)
    begin
      LoadViewsFromFile(Action.DataFile);
    end
  );
  Flux.Register<TSelectFilterViewAction>(Self,
    procedure(Action : TSelectFilterViewAction)
    begin
      FCurrent := Root.FindView(Action.View);
      if not assigned(FCurrent) then
        FCurrent := FRoot;
      TRefreshOpenFileAction.Post;
    end
  );

  Flux.Register<TClearFilterViewsAction>(Self,
    procedure(Action : TClearFilterViewsAction)
    begin
      FRoot.Views.Clear;
      FCurrent := FRoot;
      TRefreshOpenFileAction.Post;
    end
  );

  Flux.Register<TRemoveFilterViewAction>(Self,
    procedure(Action : TRemoveFilterViewAction)
    begin
      FRoot.RemoveView(FRoot.FindView(Action.View));
    end
  );

  Flux.Register<TReverseFilterViewAction>(Self,
    procedure(Action : TReverseFilterViewAction)
    begin
      var view := FRoot.FindView(Action.View);
      if Assigned(view) then
        view.Reverse;
    end
  );

  Flux.Register<TIncludeThreadFilterAction>(Self,
    procedure(Action : TIncludeThreadFilterAction)
    var
      View : IFilterView;
    begin
      // If current view is a thread view, then just add inclusions to that.
      // otherwise, create a sub view for thread exclusions.
      if (FCurrent <> FRoot) and
         (  (not FCurrent.IsFiltered) or
            (Length(FCurrent.ExcludeThreadIDs) > 0) or
            (Length(FCurrent.IncludeThreadIDs) > 0)
         ) then
        View := FCurrent
      else
        View := TFilterView(FCurrent).Add;

      var bFound := False;
      for var id in View.IncludeThreadIDs do
      begin
        bFound := id = Action.ThreadID;
        if bFound then
          break;
      end;
      if not bFound then
      begin
        TFilterView(View).IncludeThread(Action.ThreadID);
      end;

      FCurrent := View;
      SaveViewsToFile;
    end
  );

  Flux.Register<TExcludeThreadFilterAction>(Self,
    procedure(Action : TExcludeThreadFilterAction)
    var
      View : IFilterView;
    begin
      // If current view is a thread view, then just add exclusions to that.
      // otherwise, create a sub view for thread exclusions.
      if (FCurrent <> FRoot) and
         (  (not FCurrent.IsFiltered) or
            (Length(FCurrent.ExcludeThreadIDs) > 0) or
            (Length(FCurrent.IncludeThreadIDs) > 0)
         ) then
        View := FCurrent
      else
        View := TFilterView(FCurrent).Add;

      var bFound := False;
      for var id in View.ExcludeThreadIDs do
      begin
        bFound := id = Action.ThreadID;
        if bFound then
          break;
      end;
      if not bFound then
      begin
        TFilterView(View).ExcludeThread(Action.ThreadID);
      end;

      FCurrent := View;
      SaveViewsToFile;
    end
  );

  Flux.Register<TAddTextIncludeFilterAction>(Self,
    procedure(Action : TAddTextIncludeFilterAction)
    var
      View : IFilterView;
    begin
      // If current view is a TextInclude view, then just add exclusions to that.
      // otherwise, create a sub view for thread exclusions.
      if (FCurrent <> FRoot) and ((not FCurrent.IsFiltered) or (Length(FCurrent.TextInclusive) > 0)) then
        View := FCurrent
      else
        View := TFilterView(FCurrent).Add;

      var bFound := False;
      for var txt in View.TextInclusive do
      begin
        bFound := txt = Action.Text;
        if bFound then
          break;
      end;
      if not bFound then
      begin
        TFilterView(View).IncludeText(Action.Text);
      end;

      FCurrent := View;
      SaveViewsToFile;
    end
  );

  Flux.Register<TAddTextExcludeFilterAction>(Self,
    procedure(Action : TAddTextExcludeFilterAction)
    var
      View : IFilterView;
    begin
      // If current view is a TextExclude view, then just add exclusions to that.
      // otherwise, create a sub view for thread exclusions.
      if (FCurrent <> FRoot) and ((not FCurrent.IsFiltered) or (Length(FCurrent.TextExclusive) > 0)) then
        View := FCurrent
      else
        View := TFilterView(FCurrent).Add;

      var bFound := False;
      for var txt in View.TextExclusive do
      begin
        bFound := txt = Action.Text;
        if bFound then
          break;
      end;
      if not bFound then
      begin
        TFilterView(View).ExcludeText(Action.Text);
      end;

      FCurrent := View;
      SaveViewsToFile;
    end
  );

  Flux.Register<TSetLineFromFilterAction>(Self,
    procedure(Action : TSetLineFromFilterAction)
    var
      View : IFilterView;
    begin
      // If current view is a LineFrom view, then just add exclusions to that.
      // otherwise, create a sub view for thread exclusions.
      if (FCurrent <> FRoot) and ((not FCurrent.IsFiltered) or (FCurrent.LineFrom > DEFAULT_LINE_FROM)) then
        View := FCurrent
      else
        View := TFilterView(FCurrent).Add;

      TFilterView(View).UpdateLineFrom(Action.Line);

      FCurrent := View;
      SaveViewsToFile;
    end
  );

  Flux.Register<TSetLineToFilterAction>(Self,
    procedure(Action : TSetLineToFilterAction)
    var
      View : IFilterView;
    begin
      // If current view is a LineFrom view, then just add exclusions to that.
      // otherwise, create a sub view for thread exclusions.
      if (FCurrent <> FRoot) and ((not FCurrent.IsFiltered) or (FCurrent.LineTo < DEFAULT_LINE_TO)) then
        View := FCurrent
      else
        View := TFilterView(FCurrent).Add;

      TFilterView(View).UpdateLineTo(Action.Line);

      FCurrent := View;
      SaveViewsToFile;
    end
  );

  Flux.Register<TSetDeltaMinFilterAction>(Self,
    procedure(Action : TSetDeltaMinFilterAction)
    var
      View : IFilterView;
    begin
      // If current view is a DeltaMin view, then just add exclusions to that.
      // otherwise, create a sub view for thread exclusions.
      if (FCurrent <> FRoot) and ((not FCurrent.IsFiltered) or (FCurrent.DeltaMin > DEFAULT_DELTA_MIN)) then
        View := FCurrent
      else
        View := TFilterView(FCurrent).Add;

      TFilterView(View).UpdateDeltaMin(Action.MS);

      FCurrent := View;
      SaveViewsToFile;
    end
  );

  Flux.Register<TSetDurationMinFilterAction>(Self,
    procedure(Action : TSetDurationMinFilterAction)
    var
      View : IFilterView;
    begin
      // If current view is a DurationMin view, then just add exclusions to that.
      // otherwise, create a sub view for thread exclusions.
      if (FCurrent <> FRoot) and ((not FCurrent.IsFiltered) or (FCurrent.DurationMin > DEFAULT_DURATION_MIN)) then
        View := FCurrent
      else
        View := TFilterView(FCurrent).Add;

      TFilterView(View).UpdateDurationMin(Action.MS);

      FCurrent := View;
      SaveViewsToFile;
    end
  );

  Flux.Register<TSetDurationMaxFilterAction>(Self,
    procedure(Action : TSetDurationMaxFilterAction)
    var
      View : IFilterView;
    begin
      // If current view is a DurationMax view, then just add exclusions to that.
      // otherwise, create a sub view for thread exclusions.
      if (FCurrent <> FRoot) and ((not FCurrent.IsFiltered) or (FCurrent.DurationMax < DEFAULT_DURATION_MAX)) then
        View := FCurrent
      else
        View := TFilterView(FCurrent).Add;

      TFilterView(View).UpdateDurationMax(Action.MS);

      FCurrent := View;
      SaveViewsToFile;
    end
  );

  Flux.Register<TSetStampAfterFilterAction>(Self,
    procedure(Action : TSetStampAfterFilterAction)
    var
      View : IFilterView;
    begin
      // If current view is a StampAfter view, then just add exclusions to that.
      // otherwise, create a sub view for thread exclusions.
      if (FCurrent <> FRoot) and ((not FCurrent.IsFiltered) or (FCurrent.StampAfter > DEFAULT_STAMP_AFTER)) then
        View := FCurrent
      else
        View := TFilterView(FCurrent).Add;

      TFilterView(View).UpdateStampAfter(Action.Stamp);

      FCurrent := View;
      SaveViewsToFile;
    end
  );

  Flux.Register<TSetStampBeforeFilterAction>(Self,
    procedure(Action : TSetStampBeforeFilterAction)
    var
      View : IFilterView;
    begin
      // If current view is a StampBefore view, then just add exclusions to that.
      // otherwise, create a sub view for thread exclusions.
      if (FCurrent <> FRoot) and ((not FCurrent.IsFiltered) or (FCurrent.StampBefore > DEFAULT_STAMP_BEFORE)) then
        View := FCurrent
      else
        View := TFilterView(FCurrent).Add;

      TFilterView(View).UpdateStampBefore(Action.Stamp);

      FCurrent := View;
      SaveViewsToFile;
    end
  );

  Flux.Register<TSetHideProfileNoDetailFilterAction>(Self,
    procedure(Action : TSetHideProfileNoDetailFilterAction)
    var
      View : IFilterView;
    begin
      // If current view is a HideProfile view, then just add exclusions to that.
      // otherwise, create a sub view for thread exclusions.
      if (FCurrent <> FRoot) and ((not FCurrent.IsFiltered) or (FCurrent.HideProfileNoDetail)) then
        View := FCurrent
      else
        View := TFilterView(FCurrent).Add;

      TFilterView(View).HideProfileWithNoDetail;

      FCurrent := View;
      SaveViewsToFile;
    end
  );

  Flux.Register<TReparentFilterViewAction>(Self,
    procedure(Action : TReparentFilterViewAction)
    begin
      if Action.View.Parent = Action.NewParent then
        exit;
      if Action.View.Parent = nil then
        exit;
      if Action.NewParent = nil then
        exit;

      TFilterView(Action.View).ReparentTo(Action.NewParent);
      FCurrent := Action.NewParent;
    end
  );

end;

class constructor TViewsStore.Create;
begin
  FData := TViewsStore.Create;
end;

destructor TViewsStore.Destroy;
begin
  inherited;
end;

class destructor TViewsStore.Destroy;
begin
  FData.Free;
end;

procedure TViewsStore.LoadViewsFromFile(DataFile: TFileRecord);
var
  jso : IJSONObject;
begin
  FViewFilename := DataFile.FilePath+'.viewer';
  if FileExists(FViewFilename) then
    jso := TJSON.FromFile(FViewFilename)
  else
    jso := TJSON.New;
  LoadViewsFromJSON(jso);
end;

procedure TViewsStore.LoadViewsFromJSON(Data: IJSONObject);
begin
  TFilterView(FRoot).UpdateFromJSON(Data);
end;

function TViewsStore.PersistViewsToJSON: IJSONObject;
begin
  Result := TFilterView(FRoot).AsJSON(FCurrent);
end;

procedure TViewsStore.SaveViewsToFile;
var
  jso : IJSONObject;
begin
  if FViewFilename <> '' then
  begin
    jso := PersistViewsToJSON;
    jso.SaveToFile(FViewFilename);
  end;
end;

{ TViewsStore.TFilterView }

function TViewsStore.TFilterView.Add: IFilterView;
begin
  Result := TViewsStore.TFilterView.Create(Self);
  FViews.Add(Result);
end;

procedure TViewsStore.TFilterView.ApplyFrom(const AView: IFilterView);
begin
  if FStampAfter < AView.StampAfter then
    FStampAfter := AView.StampAfter;

  if FStampBefore > AView.StampBefore then
    FStampBefore := AView.StampBefore;

  for var txt in AView.TextInclusive do
  begin
    var bFound := False;
    for var i := 0 to Length(FTextInclusive)-1 do
      if FTextInclusive[i].ToUpper = txt.ToUpper then
      begin
        bFound := True;
        break;
      end;
    if not bFound then
    begin
      SetLength(FTextInclusive, Length(FTextInclusive)+1);
      FTextInclusive[Length(FTextInclusive)-1] := txt;
    end;
  end;

  for var txt in AView.TextExclusive do
  begin
    var bFound := False;
    for var i := 0 to Length(FTextExclusive)-1 do
      if FTextExclusive[i].ToUpper = txt.ToUpper then
      begin
        bFound := True;
        break;
      end;
    if not bFound then
    begin
      SetLength(FTextExclusive, Length(FTextExclusive)+1);
      FTextExclusive[Length(FTextExclusive)-1] := txt;
    end;
  end;

  for var ic in AView.IncludeThreadIDs do
  begin
    var bFound := False;
    for var i := 0 to Length(FIncludeThreadIDs)-1 do
      if FIncludeThreadIDs[i] = ic then
      begin
        bFound := True;
        break;
      end;
    if not bFound then
    begin
      SetLength(FIncludeThreadIDs, Length(FIncludeThreadIDs)+1);
      FIncludeThreadIDs[Length(FIncludeThreadIDs)-1] := ic;
    end;
  end;


  for var ic in AView.ExcludeThreadIDs do
  begin
    var bFound := False;
    for var i := 0 to Length(FExcludeThreadIDs)-1 do
      if FExcludeThreadIDs[i] = ic then
      begin
        bFound := True;
        break;
      end;
    if not bFound then
    begin
      SetLength(FExcludeThreadIDs, Length(FExcludeThreadIDs)+1);
      FExcludeThreadIDs[Length(FExcludeThreadIDs)-1] := ic;
    end;
  end;

  if FLineFrom < AView.LineFrom then
    FLineFrom := AView.LineFrom;

  if FLineTo > AView.LineTo then
    FLineTo := AView.LineTo;

  if FDurationMin < AView.DurationMin then
    FDurationMin := AView.DurationMin;

  if FDurationMax > AView.DurationMax then
    FDurationMax := AView.DurationMax;

  if FDeltaMin < AView.DeltaMin then
    FDeltaMin := AView.DeltaMin;

  if not FHideProfileNoDetail then
    FHideProfileNoDetail := AView.HideProfileNoDetail;
end;

function TViewsStore.TFilterView.AsJSON(Current : IFilterView): IJSONObject;
begin
  Result := TJSON.New;
  Result.Dates['stamp_after'] := FStampAfter;
  Result.Dates['stamp_before'] := FStampBefore;
  Result.Arrays['text_inclusive'] := TJSONArray.From<string>(FTextInclusive);
  Result.Arrays['text_exclusive'] := TJSONArray.From<string>(FTextExclusive);
  Result.Arrays['thread_inclusive'] := TJSONArray.From<UInt64>(FIncludeThreadIDs);
  Result.Arrays['thread_exclusive'] := TJSONArray.From<UInt64>(FExcludeThreadIDs);
  Result.Integers['line_from'] := FLineFrom;
  Result.Integers['line_to'] := FLineTo;
  Result.Integers['duration_min'] := FDurationMin;
  Result.Integers['duration_max'] := FDurationMax;
  Result.Integers['delta_min'] := FDeltaMin;
  Result.Booleans['hide_profile_no_data'] := FHideProfileNoDetail;

  var ary := TJSONArray.New;
  for var v in FViews do
  begin
    ary.Add(TViewsStore.TFilterView(v).AsJSON(Current));
  end;
  Result.Arrays['views'] := ary;

  Result.Booleans['is_current'] := (Current = IFilterView(Self));
end;

function TViewsStore.TFilterView.AsText: string;
begin
  Result := '';
  if Length(FIncludeThreadIDs) > 0 then
  begin
    Result := 'Include Threads: ';
    for var id in FIncludeThreadIDs do
      Result := Result+id.ToString+' ';
  end;

  if Length(FExcludeThreadIDs) > 0 then
  begin
    Result := 'Exclude Threads: ';
    for var id in FExcludeThreadIDs do
      Result := Result+id.ToString+' ';
  end;

  if Length(FTextInclusive) > 0 then
  begin
    Result := 'Include';
    for var txt in FTextInclusive do
      Result := Result + ': ' + txt;
  end;

  if Length(FTextExclusive) > 0 then
  begin
    Result := 'Exclude ';
    for var txt in FTextExclusive do
      Result := Result + ': ' + txt;
  end;

  if FStampAfter > DEFAULT_STAMP_AFTER then
    Result := 'After '+DateFormat(StampAfter);

  if FStampBefore < DEFAULT_STAMP_BEFORE then
    Result := 'Before '+DateFormat(StampAfter);

  if FLineFrom > DEFAULT_LINE_FROM then
    Result := 'Line > '+FLineFrom.ToString;

  if FLineTo < DEFAULT_LINE_TO then
    Result := 'Line < '+FLineTo.ToString;

  if FDeltaMin > DEFAULT_DELTA_MIN then
    Result := 'Delta >= '+FDeltaMin.ToString;

  if FDurationMin > DEFAULT_DURATION_MIN then
    Result := 'Duration >= '+FDurationMin.ToString;

  if FDurationMax > DEFAULT_DURATION_MAX then
    Result := 'Duration <= '+FDurationMax.ToString;

  if FHideProfileNoDetail then
    Result := 'Hide Profiles';
end;

function TViewsStore.TFilterView.CompositedView: IFilterView;
var
  Chain : TList<IFilterView>;
  obj : IFilterView;
  res : TViewsStore.TFilterView;
begin
  res := TViewsStore.TFilterView.Create(nil);
  try
    Chain := TList<IFilterView>.Create;
    try
      // Build Filter Chain
      obj := nil;
      repeat
        if obj = nil then
          obj := Self
        else
          obj := Self.Parent;
        Chain.Add(obj);
      until obj.Parent = nil;

      for var i := Chain.Count-1 downto 0 do
      begin
        res.ApplyFrom(Chain[i]);
      end;
    finally
      Chain.Free;
    end;
  except
    res.Free;
    raise;
  end;
  Result := res;
end;

constructor TViewsStore.TFilterView.Create(const AParent: IFilterView);
begin
  inherited Create;
  FParent := AParent;
  FViews := TList<IFilterView>.Create;

  FStampAfter := DEFAULT_STAMP_AFTER;
  FStampBefore := DEFAULT_STAMP_BEFORE;
  SetLength(FTextInclusive,0);
  SetLength(FTextExclusive,0);
  SetLength(FIncludeThreadIDs,0);
  SetLength(FExcludeThreadIDs,0);
  FLineFrom := DEFAULT_LINE_FROM;
  FLineTo := DEFAULT_LINE_TO;
  FDurationMin := DEFAULT_DURATION_MIN;
  FDurationMax := DEFAULT_DURATION_MAX;
  FDeltaMin := DEFAULT_DELTA_MIN;
  FHideProfileNoDetail := DEFAULT_HIDE_PROFILE;
end;

function TViewsStore.TFilterView.DateFormat(DT: TDateTime): string;
begin
  DateTimeToString(Result, 'yyyy-mm-dd hh-nn-ss.zzz', DT);
end;

destructor TViewsStore.TFilterView.Destroy;
begin
  FParent := nil;
  FViews.Free;
  inherited;
end;

procedure TViewsStore.TFilterView.ExcludeText(const Text: string);
begin
  var bFound := False;
  for var txt in FTextExclusive do
  begin
    bFound := txt.ToUpper = Text.ToUpper;
    if bFound then
      break;
  end;

  if not bFound then
  begin
    SetLength(FTextExclusive, Length(FTextExclusive)+1);
    FTextExclusive[Length(FTextExclusive)-1] := Text;
  end;
end;

procedure TViewsStore.TFilterView.IncludeThread(ID: UInt64);
begin
  SetLength(FExcludeThreadIDs,0);
  var bFound := False;
  for var thread in FIncludeThreadIDs do
  begin
    bFound := thread = ID;
    if bFound then
      break;
  end;

  if not bFound then
  begin
    SetLength(FIncludeThreadIDs, Length(FIncludeThreadIDs)+1);
    FIncludeThreadIDs[Length(FIncludeThreadIDs)-1] := id;
  end;

end;

procedure TViewsStore.TFilterView.ExcludeThread(ID: UInt64);
begin
  SetLength(FIncludeThreadIDs,0);
  var bFound := False;
  for var thread in FExcludeThreadIDs do
  begin
    bFound := thread = ID;
    if bFound then
      break;
  end;

  if not bFound then
  begin
    SetLength(FExcludeThreadIDs, Length(FExcludeThreadIDs)+1);
    FExcludeThreadIDs[Length(FExcludeThreadIDs)-1] := id;
  end;
end;

function TViewsStore.TFilterView.FindView(const AView: IFilterView): IFilterView;
begin
  Result := nil;
  var idx := FViews.IndexOf(AView);
  if idx < 0 then
  begin
    for var i := 0 to FViews.Count-1 do
    begin
      Result := FViews[i].FindView(AView);
      if (Result <> IFilterView(Self)) then
        break;
    end;
  end else
    Result := AView;

end;

function TViewsStore.TFilterView.GetDeltaMin: UInt64;
begin
  Result := FDeltaMin;
end;

function TViewsStore.TFilterView.GetDurationMax: UInt64;
begin
  Result := FDurationMax;
end;

function TViewsStore.TFilterView.GetDurationMin: UInt64;
begin
  Result := FDurationMin;
end;

function TViewsStore.TFilterView.GetHideProfileNoDetail: boolean;
begin
  Result := FHideProfileNoDetail;
end;

function TViewsStore.TFilterView.GetLineFrom: UInt64;
begin
  Result := FLineFrom;
end;

function TViewsStore.TFilterView.GetLineTo: UInt64;
begin
  Result := FLineTo;
end;

function TViewsStore.TFilterView.GetParent: IFilterview;
begin
  Result := FParent;
end;

function TViewsStore.TFilterView.GetStampAfter: TDateTime;
begin
  Result := FStampAfter;
end;

function TViewsStore.TFilterView.GetStampBefore: TDateTime;
begin
  Result := FStampBefore;
end;

function TViewsStore.TFilterView.GetTextExclusive: TArray<string>;
begin
  Result := FTextExclusive;
end;

function TViewsStore.TFilterView.GetTextInclusive: TArray<string>;
begin
  Result := FTextInclusive;
end;

function TViewsStore.TFilterView.GetIncludeThreadIDs: TArray<UInt64>;
begin
  Result := FIncludeThreadIDs;
end;

function TViewsStore.TFilterView.GetExcludeThreadIDs: TArray<UInt64>;
begin
  Result := FExcludeThreadIDs;
end;

function TViewsStore.TFilterView.GetViews: TList<IFilterView>;
begin
  Result := FViews;
end;

procedure TViewsStore.TFilterView.HideProfileWithNoDetail;
begin
  FHideProfileNoDetail := True;
end;

procedure TViewsStore.TFilterView.IncludeText(const Text: string);
begin
  var bFound := False;
  for var txt in FTextInclusive do
  begin
    bFound := txt.ToUpper = Text.ToUpper;
    if bFound then
      break;
  end;

  if not bFound then
  begin
    SetLength(FTextInclusive, Length(FTextInclusive)+1);
    FTextInclusive[Length(FTextInclusive)-1] := Text;
  end;
end;

function TViewsStore.TFilterView.IsFiltered: boolean;
begin
  Result := not (
    (Length(FIncludeThreadIDs) = 0) and
    (Length(FExcludeThreadIDs) = 0) and
    (Length(FTextInclusive) = 0) and
    (Length(FTextExclusive) = 0) and
    (FStampBefore = DEFAULT_STAMP_BEFORE) and
    (FStampAfter = DEFAULT_STAMP_AFTER) and
    (FDurationMin = DEFAULT_DURATION_MIN) and
    (FDurationMax = DEFAULT_DURATION_MAX) and
    (FLineFrom = DEFAULT_LINE_FROM) and
    (FLineTo = DEFAULT_LINE_TO) and
    (FDeltaMin = DEFAULT_DELTA_MIN) and
    (FHideProfileNoDetail = DEFAULT_HIDE_PROFILE)
  );
end;

function TViewsStore.TFilterView.IsThreadIncluded(ID: UInt64): boolean;
begin
  // First see if any threads are excluded. Result will be true if not.
  Result := Length(FExcludeThreadIDs) = 0;

  // if threds were excluded, then let's see if this thread id is in that excluded
  // list. Set result to true if it isn't.
  if not Result then
  begin
    var bIsExcluded : boolean := false;
    for var i in FExcludeThreadIDs do
    begin
      bIsExcluded := i = ID;
      if bIsExcluded then
        break;
    end;
    Result := not bIsExcluded;
  end;

  // if the thread has been explicitly excluded we can exit now.
  if not Result then
    exit;

  // if there are no included thread IDs we assume all are included.
  Result := Length(FIncludeThreadIDs) = 0;

  // if all are included we can exit now
  if Result then
    exit;

  // Exit with True if the thread is in the include list.
  for var i in FIncludeThreadIDs do
  begin
    if i = ID then
      exit(True);
  end;
end;

procedure TViewsStore.TFilterView.Remove(const AView: IFilterView);
begin
  FViews.Extract(AView);
end;

procedure TViewsStore.TFilterView.RemoveView(const AView: IFilterView);
begin
  var idx := FViews.IndexOf(AView);
  if idx < 0 then
  begin
    for var i := 0 to FViews.Count-1 do
    begin
      FViews[i].RemoveView(AView); // TODO: Optimize by breaking loop if removed from child
    end;
  end else
    FViews.Delete(idx);
end;

procedure TViewsStore.TFilterView.ReparentTo(const View: IFilterView);
begin
  FParent := View;
end;

procedure TViewsStore.TFilterView.Reverse;
begin
  var dtAfter := FStampAfter;
  var dtBefore := FStampBefore;

  var txtInclude := FTextInclusive;
  var txtExclude := FTextExclusive;
  var threadEx := FExcludeThreadIDs;
  var threadIn := FIncludeThreadIDs;
  var iFrom := FLineFrom;
  var iTo := FLineTo;
  var iMin := FDurationMin;
  var iMax := FDurationMax;

  if (dtAfter <> DEFAULT_STAMP_AFTER) then
  begin
    FStampBefore := dtAfter;
    FStampAfter := DEFAULT_STAMP_AFTER;
  end;

  if (dtBefore <> DEFAULT_STAMP_BEFORE) then
  begin
    FStampAfter := dtBefore;
    if (FStampBefore <> DEFAULT_STAMP_BEFORE) and (FStampBefore <> dtAfter) then
      FStampBefore := DEFAULT_STAMP_BEFORE;
  end;

  FIncludeThreadIDs := ThreadEx;
  FExcludeThreadIDs := ThreadIn;

  if (iFrom <> DEFAULT_LINE_FROM) then
  begin
    FLineTo := iFrom;
    FLineFrom := DEFAULT_LINE_FROM;
  end;

  if (iTo <> DEFAULT_LINE_TO) then
  begin
    FLineFrom := iTo;
    if (FLineTo <> DEFAULT_LINE_TO) and (FLineTo <> iFrom) then
      FLineTo := DEFAULT_LINE_TO;
  end;

  if (iMin <> DEFAULT_DURATION_MIN) then
  begin
    FDurationMax := iMin;
    FDurationMin := DEFAULT_DURATION_MIN;
  end;

  if (iMax <> DEFAULT_DURATION_MAX) then
  begin
    FDurationMin := iMax;
    if (FDurationMax <> DEFAULT_DURATION_MAX) and (FDurationMax <> iMin) then
      FDurationMax := DEFAULT_DURATION_MAX;
  end;

end;

procedure TViewsStore.TFilterView.UpdateDeltaMin(Value: UInt64);
begin
  FDeltaMin := Value;
end;

procedure TViewsStore.TFilterView.UpdateDurationMax(Value: UInt64);
begin
  FDurationMax := Value;
end;

procedure TViewsStore.TFilterView.UpdateDurationMin(Value: UInt64);
begin
  FDurationMin := Value;
end;

procedure TViewsStore.TFilterView.UpdateFromJSON(const Data: IJSONObject);
begin
  FViews.Clear;
  if Data.Has['views'] then
    Data.Arrays['views'].Each(
      procedure(const Data : IJSONObject)
      begin
        var v := TViewsStore.TFilterView.Create(Self);
        v.UpdateFromJSON(Data);
      end
    );

  if Data.Has['stamp_after'] then
    FStampAfter := Data.Dates['stamp_after'];

  if Data.Has['stamp_before'] then
    FStampBefore := Data.Dates['stamp_before'];

  if Data.Has['text_inclusive'] then
    FTextInclusive := Data.Arrays['text_inclusive'].AsArrayOfStrings;

  if Data.Has['text_exclusive'] then
    FTextExclusive := Data.Arrays['text_exclusive'].AsArrayOfStrings;

  if Data.Has['thread_inclusive'] then
  begin
    var ary := Data.Arrays['thread_inclusive'].AsArrayOfIntegers;
    SetLength(FIncludeThreadIDs, Length(ary));
    for var i := 0 to Length(ary)-1 do
    begin
      FIncludeThreadIDs[i] := ary[i];
    end;
  end;

  if Data.Has['thread_exclusive'] then
  begin
    var ary := Data.Arrays['thread_exclusive'].AsArrayOfIntegers;
    SetLength(FExcludeThreadIDs, Length(ary));
    for var i := 0 to Length(ary)-1 do
    begin
      FExcludeThreadIDs[i] := ary[i];
    end;
  end;

  if Data.Has['line_from'] then
    FLineFrom := Data.Integers['line_from'];

  if Data.Has['line_to'] then
    FLineTo := Data.Integers['line_to'];

  if Data.Has['duration_min'] then
    FDurationMin := Data.Integers['duration_min'];

  if Data.Has['duration_max'] then
    FDurationMax := Data.Integers['duration_max'];

  if Data.Has['delta_min'] then
    FDeltaMin := Data.Integers['delta_min'];

  if Data.Has['hide_profile_no_data'] then
    FHideProfileNoDetail := Data.Booleans['hide_profile_no_data'];

  if Data.Has['is_current'] and Data.Booleans['is_current'] then
    Async.DoLater(
      procedure
      begin
        TSelectFilterViewAction.Post(Self);
      end
    );
end;

procedure TViewsStore.TFilterView.UpdateLineFrom(Line: UInt64);
begin
  FLineFrom := Line;
end;

procedure TViewsStore.TFilterView.UpdateLineTo(Line: UInt64);
begin
  FLineTo := Line;
end;

procedure TViewsStore.TFilterView.UpdateStampAfter(Stamp: TDateTime);
begin
  FStampAfter := Stamp;
end;

procedure TViewsStore.TFilterView.UpdateStampBefore(Stamp: TDateTIme);
begin
  FStampBefore := Stamp;
end;

end.
