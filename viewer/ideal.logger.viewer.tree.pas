﻿unit ideal.logger.viewer.tree;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Types,
  System.UITypes,
  System.Generics.Collections,
  FMX.Controls,
  FMX.Layouts,
  FMX.StdCtrls;

type
  TTreeRow = record
    Expanded : Boolean;
    Text : TArray<String>;
    BackgroundColor : TAlphaColor;
    ChildCount : Cardinal;
    LineNumber : Cardinal;
    Path : TArray<Cardinal>;
    Level : Cardinal;

    class function Initialize(Path : TArray<Cardinal>; Level, ColCount : Cardinal) : TTreeRow; static;
  end;

  TOnSelectItemEvent = procedure(Sender : TObject; Path : TArray<Cardinal>) of object;
  TOnExpandItemEvent = procedure(Sender : TObject; Path : TArray<Cardinal>; Expanded : boolean) of object;
  TOnChildCountNeededEvent = procedure(Sender : TObject; Path : TArray<Cardinal>; out LineNumber, Count : Cardinal) of object;
  TOnTextNeededEvent = procedure(Sender : TObject; Path : TArray<cardinal>; var LineNumber : Cardinal; var Text : TArray<String>; var BackgroundColor : TAlphaColor; var ChildCount : Cardinal) of object;
  TLogTree = class(TLayout)
  private
    type
      TOnChildCountHandler = reference to procedure(Path : TArray<Cardinal>; out LineNumber, Count : Cardinal);
      TOnTextNeededHandler = reference to procedure(Path : TArray<cardinal>; var LineNumber : Cardinal; var Text : TArray<String>; var BackgroundColor : TAlphaColor; var ChildCount : Cardinal);
      TOnChildCountNeededHandler = reference to procedure(Path : TArray<Cardinal>; out LineNumber, Count : Cardinal);
      TOnSelectItemHandler = reference to procedure(Path : TArray<Cardinal>);
      TOnExpandItemHandler = reference to procedure(Path : TArray<Cardinal>; Expanded : boolean);
      TTreeContent = class(TControl)
      private
        FTopLine: Cardinal;
        FOnSelectItem : TOnSelectItemHandler;
        FOnTextNeeded : TOnTextNeededHandler;
        FOnChildCountNeeded : TOnChildCountNeededHandler;
        FOnExpandItem : TOnExpandItemHandler;
        FExpansions : TDictionary<Cardinal, Boolean>;
        FShowingLines : TArray<TTreeRow>;
        FLeftCols: TArray<Cardinal>;
        FRightCols: TArray<Cardinal>;
        FCurrentPos : TPointF;
        FScrollBar: TScrollBar;
        FRootCount: Cardinal;
      protected      
        procedure Paint; override;
        function GetTextHeight : Single;
          
        procedure DblClick; override;
        procedure Click; override;
        procedure MouseMove(Shift: TShiftState; X: Single; Y: Single); override;
        procedure Redraw;
//        function FindStartingPath : TArray<Cardinal>;
      public
        function CountTotalExpandedLines : Cardinal;
        function GetShowingLineCount : Cardinal;
        constructor Create(AOwner: TComponent); override;
        destructor Destroy; override;
      end;
  private
    FScrollBar: TScrollBar;
    FOnChildCountNeeded: TOnChildCountNeededEvent;
    FOnTextNeeded: TOnTextNeededEvent;
    FContent: TTreeContent;
    FOnSelectItem: TOnSelectItemEvent;
    FOnExpandItemEvent : TOnExpandItemEvent;
    FOnScroll : TNotifyEvent;
    procedure SetTopLine(const Value: Cardinal);
    procedure SetRootCount(const Value: Cardinal);
    procedure ScrollbarChange(Sender : TObject);
    procedure SetLeftCols(const Value: TArray<Cardinal>);
    procedure SetRightCols(const Value: TArray<Cardinal>);
    function GetLeftCols: TArray<Cardinal>;
    function GetRightCols: TArray<Cardinal>;
    function GetTopLine: Cardinal;
    function GetRootCount: Cardinal;
  protected
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure EndUpdate; override;

    property LeftCols : TArray<Cardinal> read GetLeftCols write SetLeftCols;
    property RightCols : TArray<Cardinal> read GetRightCols write SetRightCols;
    property TopLine : Cardinal read GetTopLine write SetTopLine;
    property RootCount : Cardinal read GetRootCount write SetRootCount;
    property OnChildCountNeeded : TOnChildCountNeededEvent read FOnChildCountNeeded write FOnChildCountNeeded;
    property OnTextNeeded : TOnTextNeededEvent read FOnTextNeeded write FOnTextNeeded;
    property OnSelectItem : TOnSelectItemEvent read FOnSelectItem write FOnSelectItem;
    property OnExpandItemEvent : TOnExpandItemEvent read FOnExpandItemEvent write FOnExpandItemEvent;
    property OnScroll : TNotifyEvent read FOnScroll write FOnScroll;
  end;


implementation

uses
  FMX.Graphics,
  FMX.Types;

{ TLogTree }

constructor TLogTree.Create(AOwner: TComponent);
begin
  inherited;
  FScrollBar := TScrollBar.Create(Self);
  FScrollBar.Parent := Self;
  FScrollBar.Align := TAlignLayout.Right;
  FScrollBar.Min := 0;
  FScrollBar.OnChange := ScrollbarChange;
  FScrollBar.ValueRange.Min := 0;
  FScrollBar.ValueRange.Max := 1;
  FScrollBar.ValueRange.Frequency := 1;
  FScrollBar.Orientation := TOrientation.Vertical;
  FScrollBar.Width := 30;
  FContent := TTreeContent.Create(Self);
  FContent.FScrollBar := FScrollBar;
  FContent.Parent := Self;
  FContent.Align := TAlignLayout.Client;

  FContent.FOnExpandItem := 
    procedure(Path : TArray<Cardinal>; Expanded : boolean)
    begin
      if Assigned(FOnExpandItemEvent) then
        FOnExpandItemEvent(Self, Path, Expanded);
    end;

  FContent.FOnChildCountNeeded :=
    procedure(Path : TArray<Cardinal>; out LineNumber, Count : Cardinal)
    begin
      if Assigned(FOnChildCountNeeded) then
        FOnChildCountNeeded(Self, Path, LineNumber, Count);
    end;
  FContent.FOnTextNeeded :=
    procedure(Path : TArray<cardinal>; var LineNumber : Cardinal; var Text : TArray<String>; var BackgroundColor : TAlphaColor; var ChildCount : Cardinal)
    begin
      if Assigned(FOnTextNeeded) then
        FOnTextNeeded(Self, Path, LineNUmber, Text, BackgroundColor, ChildCount);
    end;
  FContent.FOnSelectItem := 
    procedure(Path : TArray<Cardinal>)
    begin
      if Assigned(FOnSelectItem) then
        FOnSelectItem(Self, Path);
    end;
end;

destructor TLogTree.Destroy;
begin

  inherited;
end;

procedure TLogTree.EndUpdate;
begin
  inherited;
  if FUpdating = 0 then
    FContent.Redraw;
end;

function TLogTree.GetLeftCols: TArray<Cardinal>;
begin
  Result := FContent.FLeftCols;
end;

function TLogTree.GetRightCols: TArray<Cardinal>;
begin
  Result := FContent.FRightCols;
end;

function TLogTree.GetRootCount: Cardinal;
begin
  Result := FContent.FRootCount;
end;

function TLogTree.GetTopLine: Cardinal;
begin
  Result := FContent.FTopLine;
end;

procedure TLogTree.ScrollbarChange(Sender: TObject);
begin
  TopLine := Round(FScrollBar.Value);
  FContent.Redraw;
  if Assigned(FOnScroll) then
    FOnScroll(Self);
end;

procedure TLogTree.SetLeftCols(const Value: TArray<Cardinal>);
begin
  FContent.FLeftCols := Value;
  FContent.Redraw;
end;

procedure TLogTree.SetRightCols(const Value: TArray<Cardinal>);
begin
  FContent.FRightCols := Value;
  FContent.Redraw;
end;

procedure TLogTree.SetRootCount(const Value: Cardinal);
begin
  FContent.FRootCount := Value;
  FContent.Redraw;
end;

procedure TLogTree.SetTopLine(const Value: Cardinal);
begin
  FContent.FTopLine := Value;
  FContent.Redraw;
end;

{ TTreeRow }

class function TTreeRow.Initialize(Path : TArray<Cardinal>; Level, ColCount: Cardinal): TTreeRow;
begin
  Result.ChildCount := 0;
  Result.BackgroundColor := $FFFFFFFF;
  SetLength(Result.Text,ColCount);
  SetLength(Result.Path, Length(Path));
  for var i := 0 to Length(Path)-1 do
    Result.Path[i] := Path[i];
  Result.Level := Level;
end;

{ TLogTree.TTreeContent }

procedure TLogTree.TTreeContent.Click;
var
  idx : Cardinal;
begin
  inherited;
  var h :=  GetTextHeight + 4;
  idx := Round( (FCurrentPos.Y+4) / h ) + FTopLine - 1;
  if (idx >= 0) and (idx < Length(FShowingLines)) then
  begin  
    if FShowingLines[idx].ChildCount > 0 then
    begin
      FExpansions[FShowingLines[idx].LineNumber] := not FExpansions[FShowingLines[idx].LineNumber];
      FShowingLines[idx].Expanded := FExpansions[FShowingLines[idx].LineNumber];
      FOnExpandItem(FShowingLines[idx].Path, FShowingLines[idx].Expanded);
      Redraw;
    end;
  end;
end;

constructor TLogTree.TTreeContent.Create(AOwner: TComponent);
begin
  inherited;
  FExpansions := TDictionary<Cardinal,Boolean>.Create;
end;

procedure TLogTree.TTreeContent.DblClick;
var
  idx : Cardinal;
begin
  inherited;
  var h :=  GetTextHeight + 4;
  idx := Round( (FCurrentPos.Y+4)/ h ) + FTopLine - 1;
  if (idx >= 0) and (idx < Length(FShowingLines)) then
  begin  
    FOnSelectItem(FShowingLines[idx].Path);
  end;
end;

destructor TLogTree.TTreeContent.Destroy;
begin
  FExpansions.Free;
  inherited;
end;

{function TLogTree.TTreeContent.FindStartingPath: TArray<Cardinal>;
var
  iVisCnt : Cardinal;
  iTotalCnt : Cardinal;
  i: Integer;
  iParentCnt : integer;
  stack, stackCounter : TStack<Cardinal>;
  iLastCnt : Cardinal;
begin
  if FUpdating > 0 then
    exit;

  stack := TStack<Cardinal>.Create;
  stackCounter := TStack<Cardinal>.Create;
  try
    iVisCnt := GetShowingLineCount;
    iTotalCnt := CountTotalExpandedLines;
    FScrollBar.ViewportSize := iVisCnt div 2;
    FScrollBar.Max := iTotalCnt;
    if iTotalCnt = 0 then
    begin
      SetLength(FShowingLines, 0);
      exit;
    end;
    iLastCnt := 0;
    SetLength(Result,1);
    SetLength(FShowingLines, iVisCnt);
    for i := 0 to iVisCnt - 1 do
    begin    
      if Stack.Count > 0 then
        iParentCnt := Stack.Peek
      else
        iParentCnt := 0;
      Result[Length(Result)-1] := i-iLastCnt; //iParentCnt;
      var tr := TTreeRow.Initialize(Result, Stack.Count, Length(FLeftCols)+Length(FRightCols)+1);
      FOnTextNeeded(Result, tr.LineNumber, tr.Text, tr.BackgroundColor, tr.ChildCount);
      if not FExpansions.ContainsKey(tr.LineNumber) then
        FExpansions.Add(tr.LineNumber, False);
      tr.Expanded := FExpansions[tr.LineNumber];
      if tr.Expanded then
      begin
        stackCounter.Push(tr.ChildCount);
        Stack.Push(tr.ChildCount);
        SetLength(Result, Length(Result)+1);  
        iLastCnt := i+1;     
      end else if Stack.Count > 0 then
      begin
        stackCounter.Push(stackCounter.Pop-1);
        if stackCounter.Peek = 0 then
        begin
          iLastCnt := 0;
          Stack.Pop;
          StackCounter.Pop;
          SetLength(Result, Length(Result)-1);
        end;
      end;
      FShowingLines[i] := tr;
    end;
  finally
    Stack.Free;
    StackCounter.Free;
  end;
  
end;
}
function TLogTree.TTreeContent.GetShowingLineCount : Cardinal;
begin
  Canvas.Lock;
  try
    Result := Round(Canvas.Height / Canvas.TextHeight('W'));
  finally
    Canvas.Unlock;
  end;

end;

function TLogTree.TTreeContent.GetTextHeight: Single;
begin
  Canvas.Lock;
  try
    Result := Canvas.TextHeight('W');
  finally
    Canvas.Unlock;
  end;
end;

procedure TLogTree.TTreeContent.MouseMove(Shift: TShiftState; X, Y: Single);
begin
  inherited;
  FCurrentPos := TPointF.Create(X, Y);
end;

procedure TLogTree.TTreeContent.Paint;
var
  i: Integer;
  y : Single;
  x : Single;
  w : Single;
  h : Single;
  j: Integer;
  xTxt : Single;
  Width : Integer;
  yBottom : Single;
  r : TRectF;
begin
  inherited;
  Canvas.Lock;
  try
    Canvas.BeginScene;
    try
      Canvas.Fill.Kind := TBrushKind.Solid;
      Canvas.Stroke.Kind := TBrushKind.Solid;
      Width := Canvas.Width - 30;
      h := Canvas.TextHeight('W')+4;

      Canvas.Fill.Color := TAlphaColorRec.White;
      Canvas.Stroke.Color := TAlphaColorRec.White;
      r := TRectF.Create(0, 0, Width, Canvas.Height);
      Canvas.fillrect(r, 1);

      for i := 0 to Length(FShowingLines)-1 do
      begin
        y := (h * i);
        yBottom := y+h+2;                
        x := (FShowingLines[i].level +1) * 30;

        Canvas.Fill.Color := FShowingLines[i].BackgroundColor;
        Canvas.Stroke.Color := FShowingLines[i].BackgroundColor;

        if Canvas.Fill.Color <> TAlphaColorRec.White then
        begin
          r := TRectF.Create(0, y, Width, yBottom);
          Canvas.fillrect(r, 1);
        end;

        Canvas.Fill.Color := TAlphaColorRec.Black;
        Canvas.Stroke.Color := TAlphaColorRec.Black;

        if FShowingLines[i].ChildCount > 0 then
        begin
          r := TRectF.Create((FShowingLines[i].level * 30) + 6, y+2, 24, yBottom);
          if FShowingLines[i].Expanded then
          begin
            Canvas.FillText(r, '▼',False, 1, [], TTextAlign.Leading, TTextAlign.Leading);
          end else
            Canvas.FillText(r, '▶',False, 1, [], TTextAlign.Leading, TTextAlign.Leading);
        end;

        for j := 0 to Length(FLeftCols)-1 do
        begin
          w := FLeftCols[j];
          r := TRectF.Create(x, y+2, x+w, yBottom);
          Canvas.FillText(r, FShowingLines[i].Text[j], False, 1, [], TTextAlign.Leading, TTextAlign.Leading);
          x := x+FLeftCols[j]+2;
        end;
        
        xTxt := x;
        
        x := Width-4;
        for j := Length(FRightCols)-1 downto 0 do
        begin
          w := FRightCols[j];
          x := x-w;
          r := TRectF.Create(x, y+2, x+w, yBottom);
          Canvas.FillText(r, FShowingLines[i].Text[j+Length(FLeftCols)+1], False, 1, [], TTextAlign.Leading, TTextAlign.Leading);
        end;
        
        r := TRectF.Create(xTxt, y+2, x, yBottom);
        Canvas.FillText(r, FShowingLines[i].Text[Length(FLeftCols)], False, 1, [], TTextAlign.Leading, TTextAlign.Leading);
      end;
    finally
      Canvas.EndScene;
    end;
  finally
    Canvas.Unlock;
  end;
end;

procedure TLogTree.TTreeContent.Redraw;
var
  iVisCnt : Cardinal;
  iTotalCnt : Cardinal;
  i: Integer;
  iParentCnt : integer;
  Path : TArray<Cardinal>;
  stack, stackCounter : TStack<Cardinal>;
  iLastCnt : Cardinal;
begin
  if FUpdating > 0 then
    exit;

  //Path := FindStartingPath;
  stack := TStack<Cardinal>.Create;
  stackCounter := TStack<Cardinal>.Create;
  try
    iVisCnt := GetShowingLineCount;
    iTotalCnt := CountTotalExpandedLines;
    FScrollBar.ViewportSize := iVisCnt div 2;
    FScrollBar.Max := iTotalCnt;
    if iTotalCnt = 0 then
    begin
      SetLength(FShowingLines, 0);
      exit;
    end;
    iLastCnt := 0;
    SetLength(Path,1);
    SetLength(FShowingLines, iVisCnt);
    for i := 0 to iVisCnt - 1 do
    begin    
      if Stack.Count > 0 then
        iParentCnt := Stack.Peek
      else
        iParentCnt := 0;
      Path[Length(Path)-1] := i-iLastCnt; //iParentCnt;
      var tr := TTreeRow.Initialize(Path, Stack.Count, Length(FLeftCols)+Length(FRightCols)+1);
      FOnTextNeeded(Path, tr.LineNumber, tr.Text, tr.BackgroundColor, tr.ChildCount);
      if not FExpansions.ContainsKey(tr.LineNumber) then
        FExpansions.Add(tr.LineNumber, False);
      tr.Expanded := FExpansions[tr.LineNumber];
      if tr.Expanded then
      begin
        stackCounter.Push(tr.ChildCount);
        Stack.Push(tr.ChildCount);
        SetLength(Path, Length(Path)+1);  
        iLastCnt := i+1;     
      end else if Stack.Count > 0 then
      begin
        stackCounter.Push(stackCounter.Pop-1);
        if stackCounter.Peek = 0 then
        begin
          iLastCnt := 0;
          Stack.Pop;
          StackCounter.Pop;
          SetLength(Path, Length(Path)-1);
        end;
      end;
      FShowingLines[i] := tr;
    end;
  finally
    Stack.Free;
    StackCounter.Free;
  end;

  Paint;
end;

function TLogTree.TTreeContent.CountTotalExpandedLines: Cardinal;
  function CountChildrenFor(ary : TArray<Cardinal>) : Cardinal;
  var
    i : Cardinal;
    id : Cardinal;
    iCnt : Cardinal;
  begin
    FOnChildCountNeeded(ary, id, iCnt);
    if not FExpansions.ContainsKey(id) then
    begin
      FExpansions.Add(id, False);
    end;
    if FExpansions[id] then
    begin
      if iCnt > 0 then
      begin
        SetLength(ary,Length(ary)+1);
        for i := 0 to iCnt-1 do
        begin
          ary[Length(ary)-1] := i;
          iCnt := iCnt+CountChildrenFor(ary);
        end;
      end;
      Result := iCnt;
    end else
      Result := 0;
  end;
var
  i: Integer;
  ary : TArray<Cardinal>;
begin
  Result := FRootCount;
  SetLength(ary, 1);
  for i := 0 to FRootCount-1 do
  begin
    ary[0] := i;
    
    Result := Result+CountChildrenFor(ary);
  end;
end;


end.
