program ideal.logger.viewer;

uses
  System.StartUpCopy,
  FMX.Forms,
  cocinasync.flux.fmx.views.busy,
  ideal.logger.viewer.forms.dt.main in 'ideal.logger.viewer.forms.dt.main.pas' {frmMain},
  ideal.logger.viewer.stores.files in 'ideal.logger.viewer.stores.files.pas',
  ideal.logger.viewer.actions in 'ideal.logger.viewer.actions.pas',
  ideal.logger.viewer.stores.open in 'ideal.logger.viewer.stores.open.pas' {,
  ideal.logger.viewer.types in 'ideal.logger.viewer.types.pas',
  ideal.logger.viewer.forms.details in 'ideal.logger.viewer.forms.details.pas' {frmDetails},
  ideal.logger.viewer.types in 'ideal.logger.viewer.types.pas',
  ideal.logger.viewer.forms.dt.details in 'ideal.logger.viewer.forms.dt.details.pas' {frmDetails},
  ideal.logger.viewer.tree in 'ideal.logger.viewer.tree.pas',
  ideal.logger.viewer.stores.views in 'ideal.logger.viewer.stores.views.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
