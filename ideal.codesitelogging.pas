unit ideal.codesitelogging;

interface

uses
  System.SysUtils,
  System.Classes,
  ideal.logger;

type
  TCodesiteLogger = class
  public
    class procedure Send(const msg : string); overload; inline;
    class procedure Send(const msg : string; b : boolean); overload; inline;
    class procedure Send(const msg : string; obj : TObject); overload; inline;
    class procedure Send(const msg : string; const Detail : string); overload; inline;
    class procedure SendNote(const msg : string); overload; inline;
    class procedure SendNote(const msg : string; const Detail : string); overload; inline;
    class procedure SendMsg(const msg : string); overload; inline;
    class procedure SendMsg(const msg : string; const Detail : string); overload; inline;
    class procedure SendWarning(const msg : string); inline;
    class procedure SendError(const msg : string); inline;
    class procedure SendException(const msg : string); overload; inline;
    class procedure SendException(const msg : string; E : Exception); overload; inline;
    class procedure SendException(E : Exception); overload; inline;
    class function EnterMethod(const method : string) : IScopeCheck; overload; inline;
    class procedure ExitMethod(const method : string); overload; inline;
    class function EnterMethod(const obj, method : string) : IScopeCheck; overload; inline;
    class procedure ExitMethod(const obj, method : string); overload; inline;
    class function EnterMethod(obj : TObject; const method : string) : IScopeCheck; overload; inline;
    class procedure ExitMethod(obj : TObject; const method : string); overload; inline;
    class function Installed : boolean;
  public
    constructor Create; reintroduce; overload;
    constructor Create(ANothing : TObject); reintroduce; overload;
  end;

var
  Codesite : TCodesiteLogger;

implementation

uses
  System.Diagnostics,
  System.SyncObjs,
  cocinasync.collections,
  chimera.json;

{ TCodesiteLogger }

class function TCodesiteLogger.EnterMethod(const method: string): IScopeCheck;
begin
  Result := TLogger.Profile(method);
end;

class procedure TCodesiteLogger.ExitMethod(const method: string);
begin
  TLogger.Trace('Exit '+method);
end;

class procedure TCodesiteLogger.Send(const msg: string);
begin
  TLogger.Info(msg);
end;

class procedure TCodesiteLogger.ExitMethod(const obj, method: string);
begin
  TLogger.Trace('Exit '+Obj+'.'+Method);
end;

class procedure TCodesiteLogger.Send(const msg, Detail: string);
begin
  TLogger.Info(msg, detail);
end;

class procedure TCodesiteLogger.Send(const msg: string; b: boolean);
begin
  TLogger.Info(msg, BoolToStr(b, True));
end;

class procedure TCodesiteLogger.SendException(const msg: string; E: Exception);
begin
  TLogger.Error(e, msg);
end;

class procedure TCodesiteLogger.SendException(const msg: string);
begin
  TLogger.Error(msg);
end;

class procedure TCodesiteLogger.SendWarning(const msg: string);
begin
  TLogger.Warning(msg);
end;

class procedure TCodesiteLogger.Send(const msg: string; obj: TObject);
begin
  TLogger.Info(msg, obj.ClassName+'@'+Integer(@obj).ToString);
end;

class procedure TCodesiteLogger.SendError(const msg: string);
begin
  TLogger.Error(msg);
end;

class procedure TCodesiteLogger.SendException(E: Exception);
begin
  TLogger.Error(E);
end;

class procedure TCodesiteLogger.SendMsg(const msg: string);
begin
  TLogger.Debug(msg);
end;

class procedure TCodesiteLogger.SendMsg(const msg, Detail: string);
begin
  TLogger.Debug(msg, detail);
end;

class procedure TCodesiteLogger.SendNote(const msg: string);
begin
  TLogger.Info(msg);
end;

class procedure TCodesiteLogger.SendNote(const msg, Detail: string);
begin
  TLogger.Info(msg, detail);
end;

class function TCodesiteLogger.EnterMethod(const obj, method: string): IScopeCheck;
begin
  Result := TLogger.Profile(obj+'.'+method);
end;

constructor TCodesiteLogger.Create;
begin
  inherited Create;
end;

constructor TCodesiteLogger.Create(ANothing: TObject);
begin
  inherited Create;
end;

class function TCodesiteLogger.EnterMethod(obj: TObject;
  const method: string): IScopeCheck;
begin
  Result := TLogger.Profile(obj.ClassName+'@'+Integer(@obj).ToString+'.'+method);
end;

class procedure TCodesiteLogger.ExitMethod(obj: TObject; const method: string);
begin
  TLogger.Trace('Exit '+obj.ClassName+'@'+Integer(@obj).ToString+'.'+method);
end;

class function TCodesiteLogger.Installed: boolean;
begin
  Result := False;
end;

initialization
  Codesite := TCodesiteLogger.Create;

finalization
  Codesite.Free;


end.