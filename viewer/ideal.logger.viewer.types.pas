unit ideal.logger.viewer.types;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Generics.Defaults,
  System.Generics.Collections,
  chimera.json;

{$SCOPEDENUMS ON}

type
  TLogEntryType = (DEBUG, TRACE, INFO, ERROR, WARN, LICENSE, FATAL, PROFILE);
  TLogEntryTypes = set of TLogEntryType;

  TLogEntryTypeHelper = record helper for TLogEntryType
    class function FromString(Value : string) : TLogEntryType; static;
    class function ToString(Value : TLogEntryType) : string; static;
  end;

  TLogEntryTypesHelper = record helper for TLogEntryTypes
    function IsInSet(Value : string) : boolean; overload;
    function IsInSet(Value : TLogEntryType) : boolean; overload;
    function Toggle(Value : TLogEntryType) : boolean;
    function IsEmpty : boolean;
  end;

  IFilterView = interface(IInterface)
    function GetStampAfter : TDateTime;
    function GetStampBefore : TDateTime;
    function GetTextInclusive : TArray<string>;
    function GetTextExclusive : TArray<string>;
    function GetIncludeThreadIDs : TArray<UInt64>;
    function GetExcludeThreadIDs : TArray<UInt64>;
    function GetLineFrom : UInt64;
    function GetLineTo : UInt64;
    function GetDurationMin : UInt64;
    function GetDurationMax : UInt64;
    function GetDeltaMin : UInt64;
    function GetHideProfileNoDetail : boolean;
    function GetViews : TList<IFilterView>;
    function GetParent : IFilterview;

    property StampAfter : TDateTime read GetStampAfter;
    property StampBefore : TDateTime read GetStampBefore;
    property TextInclusive : TArray<string> read GetTextInclusive;
    property TextExclusive : TArray<string> read GetTextExclusive;
    property IncludeThreadIDs : TArray<UInt64> read GetIncludeThreadIDs;
    property ExcludeThreadIDs : TArray<UInt64> read GetExcludeThreadIDs;
    property LineFrom : UInt64 read GetLineFrom;
    property LineTo : UInt64 read GetLineTo;
    property DurationMin : UInt64 read GetDurationMin;
    property DurationMax : UInt64 read GetDurationMax;
    property DeltaMin : UInt64 read GetDeltaMin;
    property HideProfileNoDetail : boolean read GetHideProfileNoDetail;
    property Views : TList<IFilterView> read GetViews;
    property Parent : IFilterView read GetParent;

    function IsFiltered : boolean;
    function IsThreadIncluded(ID : UInt64) : boolean;
    function CompositedView : IFilterView;
    function FindView(const AView : IFilterView) : IFilterView;
    procedure RemoveView(const AView : IFilterView);
    procedure Reverse;
    function AsText : string;
  end;

  TLineInfo = record
    LineNumber : UInt64;
    Msg : String;
    Thread : Int64;
    Level : String;
    Offset : String;
    Delta : Int64;
    Duration : Double;
    StartPOS : Integer;
    Length : Integer;
    TimeStamp : TDateTime;
    class function From(AStream : TStream; AStartPOS, ALength : Integer) : TLineInfo; static;
  end;

  ILineGroupInfo = interface
    function GetLineFrom : UInt64;
    function GetLineTo : UInt64;
    function GetDuration : Double;
    function GetText : string;
    function GetOffset : string;
    function GetNotes : string;
    function GetDelta : Int64;
    function GetItems : TList<ILineGroupInfo>;
    function GetTimestamp : TDateTime;
    function GetThread : Int64;
    function GetLevel : String;

    property LineFrom : UInt64 read GetLineFrom;
    property LineTo : UInt64 read GetLineTo;
    property Duration : Double read GetDuration;
    property Notes : string read GetNotes;
    property Text : string read GetText;
    property Items : TList<ILineGroupInfo> read GetItems;
    property Delta : Int64 read GetDelta;
    property Timestamp : TDateTime read GetTimestamp;
    property Thread : Int64 read GetThread;
    property Level : String read GetLevel;
    property Offset : String read GetOffset;
  end;

  TFileRecord = record
    FileName : string;
    FilenameDate : TDateTIme;
    Path : string;
    Root : string;
    Size : UInt64;
    class operator Equal(const Val1, Val2 : TFileRecord) : boolean;
    class operator NotEqual(const Val1, Val2 : TFileRecord) : boolean;
    class operator GreaterThan(const Val1, Val2 : TFileRecord) : boolean;
    class operator LessThan(const Val1, Val2 : TFileRecord) : boolean;
    class operator GreaterThanOrEqual(const Val1, Val2 : TFileRecord) : boolean;
    class operator LessThanOrEqual(const Val1, Val2 : TFileRecord) : boolean;
    class function From(const AFilepath, ARoot : string) : TFileRecord; static;
    function FilePath : string;
    function RelativeFilePath : string;
    function IsInList(List : TList<TFileRecord>) : boolean;
    class function IsValidFilename(Filename : string) : boolean; static;
  end;

  TFileComparer = class(TInterfacedObject, IComparer<TFileRecord>)
  strict private
    function Compare(const Left, Right: TFileRecord): Integer;
  end;

const
  DEFAULT_STAMP_AFTER = 0;
  DEFAULT_STAMP_BEFORE = 401769; //  3000-1-1
  DEFAULT_LINE_TO = 0;
  DEFAULT_LINE_FROM = High(UInt64);
  DEFAULT_DURATION_MIN = 0;
  DEFAULT_DURATION_MAX = High(UInt64);
  DEFAULT_DELTA_MIN = 0;
  DEFAULT_HIDE_PROFILE = False;

implementation

uses
  System.IOUtils,
  System.DateUtils;

{ TLogEntryTypeHelper }

class function TLogEntryTypeHelper.FromString(Value: string): TLogEntryType;
begin
  if Value = 'DEBUG' then
    Result := TLogEntryType.DEBUG
  else if Value = 'TRACE' then
    Result := TLogEntryType.TRACE
  else if Value = 'INFO' then
    Result := TLogEntryType.INFO
  else if Value = 'ERROR' then
    Result := TLogEntryType.ERROR
  else if Value = 'WARN' then
    Result := TLogEntryType.WARN
  else if Value = 'LICENSE' then
    Result := TLogEntryType.LICENSE
  else if Value = 'FATAL' then
    Result := TLogEntryType.FATAL
  else
    Result := TLogEntryType.PROFILE;
end;

class function TLogEntryTypeHelper.ToString(Value: TLogEntryType): string;
begin
  case Value of
    TLogEntryType.DEBUG:   Result := 'DEBUG';
    TLogEntryType.TRACE:   Result := 'TRACE';
    TLogEntryType.INFO:    Result := 'INFO';
    TLogEntryType.ERROR:   Result := 'ERROR';
    TLogEntryType.WARN:    Result := 'WARN';
    TLogEntryType.LICENSE: REsult := 'LLICENSE';
    TLogEntryType.FATAL:   Result := 'FATAL';
    TLogEntryType.PROFILE: REsult := 'PROFILE';
  end;
end;

{ TLogEntryTypesHelper }

function TLogEntryTypesHelper.IsEmpty: boolean;
begin
  Result := Self = [];
end;

function TLogEntryTypesHelper.IsInSet(Value: TLogEntryType): boolean;
begin
  Result := Value in Self;
end;

function TLogEntryTypesHelper.Toggle(Value: TLogEntryType): boolean;
begin
  if Value in Self then
    Exclude(Self, Value)
  else
    Include(Self, Value);
  Result := Value in Self;
end;

function TLogEntryTypesHelper.IsInSet(Value: string): boolean;
begin
  Result := IsInSet(TLogEntryType.FromString(Value));
end;

{ TLineInfo }

class function TLineInfo.From(AStream : TStream; AStartPOS, ALength: Integer): TLineInfo;
var
  ary : TArray<byte>;
begin
  Result.StartPOS := AStartPOS;
  Result.Length := ALength;
  var iPos := AStream.Position;
  SetLength(ary, ALength);

  AStream.Position := AStartPOS;
  AStream.Read(ary, ALength);
  AStream.Position := iPos;

  var jso := TJSON.From(TEncoding.UTF8.GetString(ary));
  Result.LineNumber := jso.Integers['@i'];
  Result.Msg := jso.Strings['@m'];
  Result.Level := jso.Strings['@l'];
  Result.Delta := jso.Integers['@ms_delta'];
  Result.Thread := jso.Integers['@thread_id'];
  Result.TimeStamp := jso.Dates['@t'];
  if jso.Has['@method_duration'] and (jso.Types['@method_duration'] = TJSONValueType.number) then
    Result.Duration := jso.Numbers['@method_duration']
  else
    Result.Duration := 0;
  Result.Offset := jso.Strings['@offset'];
end;

{ TFileRecord }

class operator TFileRecord.Equal(const Val1,
  Val2: TFileRecord): boolean;
begin
  Result := Val1.FilePath = Val2.FilePath;
end;

function TFileRecord.FilePath: string;
begin
  Result := TPath.Combine(Path, Filename);
end;

class function TFileRecord.From(const AFilepath, ARoot: string): TFileRecord;
  function ExtractFileDate(const Filename : string) : TDateTime;
  begin
    Result := 0;
    if not IsValidFilename(Filename) then
      exit;
    var ary := Filename.Split(['.']);
    var aryDT : TArray<String>;
    if ary[Length(ary)-5].Contains('_') then
      aryDT := ary[Length(ary)-5].Split(['_'])
    else
      aryDT := ary[Length(ary)-5].Split([' ']);
    var aryDate := aryDT[0].Split(['-']);
    if length(aryDate) <> 3 then
      exit;
    Result := EncodeDateTime(aryDate[0].ToInteger, aryDate[1].ToInteger, aryDate[2].ToInteger, aryDT[1].ToInteger, ary[Length(ary)-4].ToInteger, ary[Length(ary)-3].ToInteger, ary[Length(ary)-2].ToInteger);
  end;
begin
  Result.FileName := ExtractFilename(AFilepath);
  Result.Path := ExtractFilepath(AFilepath);
  Result.Root := ARoot;
  Result.FilenameDate := ExtractFileDate(Result.Filename);

  var fs := TFile.OpenRead(AFilePath);
  try
    if ExtractFileExt(AFilePath).ToLower = '.jsonz' then
    begin
      fs.Position := 4;
      fs.Read(Result.Size,4);
    end else
      Result.Size := fs.Size;
  finally
    fs.Free;
  end;
end;

class operator TFileRecord.GreaterThan(const Val1,
  Val2: TFileRecord): boolean;
begin
  Result := Val1.FilePath > Val2.FilePath;
end;

class operator TFileRecord.GreaterThanOrEqual(const Val1,
  Val2: TFileRecord): boolean;
begin
  Result := Val1.FilePath >= Val2.FilePath;
end;

function TFileRecord.IsInList(List: TList<TFileRecord>): boolean;
begin
  result := False;
  for var fi in List do
  begin
    if fi = Self then
      Exit(True);
  end;

end;

class function TFileRecord.IsValidFilename(
  Filename: string): boolean;
var
  ary : TArray<String>;
begin
  Result := Filename.EndsWith('.json') or Filename.EndsWith('.jsonz');
  if Result then
  begin
    ary := Filename.Split(['.']);
    Result := Length(ary) >= 6;
    if Result then
    begin
      Result := (Length(ary[Length(ary)-5]) = 13) and
                (Length(ary[Length(ary)-4]) = 2) and
                (Length(ary[Length(ary)-3]) = 2) and
                (Length(ary[Length(ary)-2]) = 3);

    end;

  end;
end;

class operator TFileRecord.LessThan(const Val1,
  Val2: TFileRecord): boolean;
begin
  Result := Val1.FilePath < Val2.FilePath;
end;

class operator TFileRecord.LessThanOrEqual(const Val1,
  Val2: TFileRecord): boolean;
begin
  Result := Val1.FilePath <= Val2.FilePath;
end;

class operator TFileRecord.NotEqual(const Val1,
  Val2: TFileRecord): boolean;
begin
  Result := Val1.FilePath <> Val2.FilePath;
end;

function TFileRecord.RelativeFilePath: string;
begin
  Result := FilePath.Replace(Root,'');
end;

{ TFileComparer }

function TFileComparer.Compare(const Left,
  Right: TFileRecord): Integer;
begin
  Result := -1 * CompareText(Left.Filename, Right.FileName);
end;

end.
